#include "Header/cvimagecontainer.h"

void CVImageContainer::loadImage(std::string path){
    this->image = cv::imread(path,cv::IMREAD_GRAYSCALE);
    this->changedImage = this->image.clone();
    this->imagesLoaded = true;
}


void CVImageContainer::showOriginalImage(){
    if(imagesLoaded){
        cv::namedWindow("Original Image",cv::WINDOW_GUI_EXPANDED | cv::WINDOW_AUTOSIZE);
        cv::imshow("Original Image",this->image);
    }

}
void CVImageContainer::showChangedImage(){
    if(imagesLoaded){
        cv::namedWindow("Changed Image",cv::WINDOW_GUI_EXPANDED | cv::WINDOW_AUTOSIZE);
        cv::imshow("Changed Image",this->changedImage);
    }
}
