#ifndef CVIMAGEWIDGET_H
#define CVIMAGEWIDGET_H
#endif // CVIMAGEWIDGET_H

#include <QWidget>
#include <QImage>
#include <QPainter>
#include <opencv2/opencv.hpp>

class CVImageWidget : public QWidget
{
    Q_OBJECT
public:

    explicit CVImageWidget(QWidget *parent = 0) : QWidget(parent) {}

    QSize sizeHint() const { return _qimage.size(); }
    QSize minimumSizeHint() const { return _qimage.size(); }

public slots:

    void showImage(const cv::Mat& image);

protected:

    void paintEvent(QPaintEvent* /*event*/) {
        painter.begin(this);
        painter.drawImage(QPoint(0,0), _qimage);
        painter.end();
    }
    QPainter painter;
    QImage _qimage;
    cv::Mat _tmp;
};

