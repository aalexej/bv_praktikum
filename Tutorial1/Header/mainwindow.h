#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QLabel>
#include <QMenu>
#include <QMenuBar>
#include <QFileDialog>
#include <QChartView>
#include <QSpinBox>
#include <QGroupBox>
#include <QDoubleSpinBox>


#include "ui_mainwindow.h"
#include <opencv2/opencv.hpp>
#include "Header/colorspace.h"
#include "Header/histogram.h"
#include "Header/convolutionwidget.h"
#include <iostream>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    void setupPropElements(QVBoxLayout *propsLayout);
    void setupMenuBarOwn(QMenuBar *menuBar);
    void setupUIOwn(QTabWidget *window);
    void updateChart();
    void addSpinBoxes(QVBoxLayout *propsLayout);

    ~MainWindow();


public slots:
    void loadImage();
    void saveImage();
    void exitProgramm();

    void histogrammstretch();
    void gammacorrection();
    void equalizeHistogramm();
    void histstretchAndGamma();
    void histstretchAndEqualize();
    void revertSettings();

    void displayHistogramm();
    void displayImage();



private:
    Ui::MainWindow *ui;
    QtCharts::QChartView *chartView;
    cv::Mat image;
    cv::Mat original;
    QSpinBox *wminSpinBox;
    QSpinBox *wmaxSpinBox;
    QDoubleSpinBox *gammaSpinBox;
    CVImageWidget * cvimagewidget;
    ConvolutionWidget *convolutionWidget;

};
#endif // MAINWINDOW_H
