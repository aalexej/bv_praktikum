#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QApplication>
#include <opencv2/core/core.hpp>
#include "Header/CVImageContainer.h"
#include <QFileDialog>
#include <QMenuBar>
#include <QVBoxLayout>
#include <QPushButton>
#include <QMessageBox>
#include <QString>

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    void setupMenuBarOwn();
    void setupControlls();
    ~MainWindow();

public slots:
    void loadImage();
    void exitProgramm();
    void aufgabeArun();
    void aufgabeBrun();


private:
    void showWarningDialog(QString text);
    QWidget *centralWidget;
    CVImageContainer imageContainer;
};
#endif // MAINWINDOW_H
