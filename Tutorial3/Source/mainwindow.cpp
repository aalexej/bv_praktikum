/*    Aufgaben von BV_P1
 * 1. Lesen Sie ein beliebiges Bild ein
 * 2. Stellen Sie das Bild einem Fenster dar (imshow)
 * 3. Zerlegen Sie das Bild in seine drei Kanäle und zeigen Sie diese an OpenCV: split(I, channels)
 * 4. Erzeugen Sie zwei Bilder in einem Abstand von c.a 6,5cm und generieren Sie daraus ein Anaglyphenbild
 * 5. Konvertieren Sie das Bild in verschiedene Farbräume und zeigen Sie jeweils das Ergebnis und die Kanäle an
 * 6. Ausgaben vorbereiten, dass man die verschiedenen Farbssysteme nacheinander zeigen kann
 */

/* Standart Color-Space von OpenCV ist BGR
 */

#include "Header/mainwindow.h"


const cv::String waldBild = "/home/andrei/Dokumente/QtProjects/BV_Praktikum/Tutorial1/Bilder/wald.jpg";
const cv::String lampeLinks = "/home/andrei/Dokumente/QtProjects/BV_Praktikum/Tutorial1/Bilder/lampe_links.jpg";
const cv::String lampeRechts = "/home/andrei/Dokumente/QtProjects/BV_Praktikum/Tutorial1/Bilder/lampe_rechts.jpg";
const cv::String kindBild = "/home/andrei/Dokumente/QtProjects/BV_Praktikum/Tutorial1/Bilder/kind.png";



void MainWindow::setupPropElements(QVBoxLayout *propsLayout){


    QPushButton *stretchApplyButton = new QPushButton("Histogrammstretch");
    QObject::connect(stretchApplyButton,SIGNAL(clicked()),this,SLOT(histogrammstretch()));

    QPushButton *gammaApplyButton = new QPushButton("Gammakorrektur");
    QObject::connect(gammaApplyButton,SIGNAL(clicked()),this,SLOT(gammacorrection()));
    QPushButton *equalizeApplyButton = new QPushButton("Linearisierung");
    QObject::connect(equalizeApplyButton,SIGNAL(clicked()),this,SLOT(equalizeHistogramm()));
    QPushButton *stretchGammaApplyButton = new QPushButton("Stretch+Gammakorrektur");
    QObject::connect(stretchGammaApplyButton,SIGNAL(clicked()),this,SLOT(histstretchAndGamma()));
    QPushButton *stretchEqualizeApplyButton = new QPushButton("Stretch+Linearisierung");
    QObject::connect(stretchEqualizeApplyButton,SIGNAL(clicked()),this,SLOT(histstretchAndEqualize()));
    QPushButton *resetEverything = new QPushButton("Zurücksetzen");
    QObject::connect(resetEverything,SIGNAL(clicked()),this, SLOT(revertSettings()));

    //QObject::connect(stretchApplyButton,SIGNAL(clicked()),this,SLOT(printSimpleText()));
    addSpinBoxes(propsLayout);
    QGroupBox *functionGroup = new QGroupBox(tr("Funktionen"));
    QVBoxLayout *groupLayout = new QVBoxLayout;

    groupLayout->addWidget(stretchApplyButton,0,Qt::AlignTop);
    groupLayout->addWidget(gammaApplyButton,0,Qt::AlignTop);
    groupLayout->addWidget(equalizeApplyButton,0,Qt::AlignTop);
    groupLayout->addWidget(stretchGammaApplyButton,0,Qt::AlignTop);
    groupLayout->addWidget(stretchEqualizeApplyButton,0,Qt::AlignTop);

    functionGroup->setLayout(groupLayout);

    propsLayout->addWidget(functionGroup,0,Qt::AlignTop);
    propsLayout->addWidget(resetEverything,0,Qt::AlignBottom|Qt::AlignHCenter);


}
void MainWindow::addSpinBoxes(QVBoxLayout *propsLayout){


    QGroupBox *spinBoxGroup = new QGroupBox(tr("Histogrammeinstellungen"));
    QVBoxLayout *groupLayout = new QVBoxLayout;
    groupLayout->setSizeConstraint(QLayout::SetMinAndMaxSize);

    //spinBoxGroup->setSizePolicy()
    QLabel  *wminLabel = new QLabel(QString(tr("Zahl zwischen %1 und %2").arg(0).arg(255)));
    wminSpinBox = new QSpinBox;
    wminSpinBox->setRange(0,255);
    QLabel  *wmaxLabel = new QLabel(QString(tr("Zahl zwischen %1 und %2").arg(0).arg(255)));
    wmaxSpinBox = new QSpinBox;
    wmaxSpinBox->setRange(0,255);
    QLabel  *gammaLabel = new QLabel(QString(tr("Zahl zwischen %1 und %2").arg(0.1).arg(3.0)));
    gammaSpinBox = new QDoubleSpinBox;
    gammaSpinBox->setRange(0.1,3.0);
    gammaSpinBox->setValue(1.0);
    gammaSpinBox->setSingleStep(0.1);

    groupLayout->addWidget(wminLabel,0,Qt::AlignTop);
    groupLayout->addWidget(wminSpinBox,0,Qt::AlignTop);
    groupLayout->addWidget(wmaxLabel,0,Qt::AlignTop);
    groupLayout->addWidget(wmaxSpinBox,0,Qt::AlignTop);
    groupLayout->addWidget(gammaLabel,0,Qt::AlignTop);
    groupLayout->addWidget(gammaSpinBox,0,Qt::AlignTop);
    spinBoxGroup->setLayout(groupLayout);
    propsLayout->addWidget(spinBoxGroup,0,Qt::AlignTop);

}

void MainWindow::setupMenuBarOwn(QMenuBar *menuBar){
    QMenu *fileMenu = new QMenu("Datei");
    menuBar->addMenu(fileMenu);
    fileMenu->addAction("&Bild laden...",this,SLOT(loadImage()));
    fileMenu->addAction("Bild speichern...");
    fileMenu->addSeparator();
    fileMenu->addAction("Beenden",this,SLOT(exitProgramm()));

}

void MainWindow::updateChart(){
    QtCharts::QChart* chart = createChartFromHistogramm(image);
    chartView->setChart(chart);
}


void MainWindow::setupUIOwn(QTabWidget *window){
    chartView = new QtCharts::QChartView;
    convolutionWidget = new ConvolutionWidget;
    QHBoxLayout *uiLayout = new QHBoxLayout;
    QWidget *propsPage = new QWidget;




    QWidget *props = new QWidget;
    QVBoxLayout *propsLayout = new QVBoxLayout;
    propsLayout->setSizeConstraint(QLayout::SetMinimumSize);
    setupPropElements(propsLayout);
    props->setLayout(propsLayout);

    uiLayout->addWidget(props,0,Qt::AlignTop|Qt::AlignLeft);
    uiLayout->addWidget(chartView);



    propsPage->setLayout(uiLayout);
    window->addTab(propsPage,QString("Histogrammwerkzeuge"));
    window->addTab(convolutionWidget,QString("Faltungsoperationen"));
}




MainWindow::MainWindow(QWidget *parent): QMainWindow(parent) , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setupMenuBarOwn(ui->menubar);
    this->setupUIOwn(ui->centralwidget);

}

MainWindow::~MainWindow()
{
    delete ui;
    delete chartView;
}



void MainWindow::loadImage(){
    QString file;
    file = QFileDialog::getOpenFileName(ui->centralwidget,
        tr("Bild auswählen"), "/home/andrei/Bilder", tr("Bilddateien (*.png *.jpg )"));


    if(!file.isEmpty()){
        std::cout << file.toStdString() << std::endl;
        this->image = cv::imread(file.toStdString());
        cv::cvtColor(image,image,CV_BGR2GRAY);
        this->original = this->image.clone();
        cv::imshow("Originalbild: ",this->original);
        this->convolutionWidget->loadImage(this->original);

        displayImage();
        updateChart();
    }


}
void MainWindow::saveImage(){

}
void MainWindow::exitProgramm(){
    QApplication::quit();
}

void MainWindow::histogrammstretch(){

    histogramstretching(this->image,wminSpinBox->value(),wmaxSpinBox->value(),1);
    updateChart();
    displayImage();
}
void MainWindow::gammacorrection(){
    histogramstretching(this->image,wminSpinBox->value(),wmaxSpinBox->value(),gammaSpinBox->value());
    updateChart();
    displayImage();
}

void MainWindow::equalizeHistogramm(){
    equalizeHist(this->image);
    updateChart();
    displayImage();
}
void MainWindow::histstretchAndGamma(){

}
void MainWindow::histstretchAndEqualize(){

}
void MainWindow::revertSettings(){
    this->image = this->original.clone();
    updateChart();
    displayImage();
}

void MainWindow::displayHistogramm(){

}
void MainWindow::displayImage(){
    cv::imshow("Histogrammbild",this->image);
}





