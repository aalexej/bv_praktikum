#include "Header/fourierImage.h"


/*
    Types of Mat
    +--------+----+----+----+----+------+------+------+------+
    |        | C1 | C2 | C3 | C4 | C(5) | C(6) | C(7) | C(8) |
    +--------+----+----+----+----+------+------+------+------+
    | CV_8U  |  0 |  8 | 16 | 24 |   32 |   40 |   48 |   56 |
    | CV_8S  |  1 |  9 | 17 | 25 |   33 |   41 |   49 |   57 |
    | CV_16U |  2 | 10 | 18 | 26 |   34 |   42 |   50 |   58 |
    | CV_16S |  3 | 11 | 19 | 27 |   35 |   43 |   51 |   59 |
    | CV_32S |  4 | 12 | 20 | 28 |   36 |   44 |   52 |   60 |
    | CV_32F |  5 | 13 | 21 | 29 |   37 |   45 |   53 |   61 |
    | CV_64F |  6 | 14 | 22 | 30 |   38 |   46 |   54 |   62 |
    +--------+----+----+----+----+------+------+------+------+
 */


void fftshift(const cv::Mat& inputImg, cv::Mat& outputImg)
{
    outputImg = inputImg.clone();
    // rearrange the quadrants of Fourier image  so that the origin is at the image center
    int cx = outputImg.cols / 2;
    int cy = outputImg.rows / 2;
    cv::Mat q0(outputImg, cv::Rect(0, 0, cx, cy));   // Top-Left - Create a ROI per quadrant
    cv::Mat q1(outputImg, cv::Rect(cx, 0, cx, cy));  // Top-Right
    cv::Mat q2(outputImg, cv::Rect(0, cy, cx, cy));  // Bottom-Left
    cv::Mat q3(outputImg, cv::Rect(cx, cy, cx, cy)); // Bottom-Right
    cv::Mat tmp;
    q0.copyTo(tmp);
    q3.copyTo(q0);
    tmp.copyTo(q3);
    q1.copyTo(tmp);
    q2.copyTo(q1);
    tmp.copyTo(q2);
}

void fourier(cv::Mat image){
        cv::Mat I = image.clone();
        cv::Mat padded;                            //expand input image to optimal size
        int m = cv::getOptimalDFTSize( I.rows );
        int n = cv::getOptimalDFTSize( I.cols ); // on the border add zero values
        cv::copyMakeBorder(I, padded, 0, m - I.rows, 0, n - I.cols, cv::BORDER_CONSTANT, cv::Scalar::all(0));

        cv::Mat planes[] = {cv::Mat_<double>(padded), cv::Mat::zeros(padded.size(), CV_64F)};
        cv::Mat complexI;
        cv::merge(planes, 2, complexI);         // Add to the expanded another plane with zeros

        cv::dft(complexI, complexI);            // this way the result may fit in the source matrix
        cv::Mat amplitudenSpektrum;
        // compute the magnitude and switch to logarithmic scale
        // => log(1 + sqrt(Re(DFT(I))^2 + Im(DFT(I))^2))
        cv::split(complexI, planes);                   // planes[0] = Re(DFT(I), planes[1] = Im(DFT(I))
        cv::magnitude(planes[0], planes[1], amplitudenSpektrum);// planes[0] = amplitudenSpektrum


        amplitudenSpektrum += cv::Scalar::all(1);                    // switch to logarithmic scale
        cv::log(amplitudenSpektrum, amplitudenSpektrum);

        // crop the spectrum, if it has an odd number of rows or columns
        amplitudenSpektrum = amplitudenSpektrum(cv::Rect(0, 0, amplitudenSpektrum.cols & -2, amplitudenSpektrum.rows & -2));

        //fftshift(amplitudenSpektrum,amplitudenSpektrum);

        normalize(amplitudenSpektrum, amplitudenSpektrum, 0, 1, CV_MINMAX); // Transform the matrix with float values into a
                                                // viewable image form (float between values 0 and 1).

        imshow("Amplitudenspektrum", amplitudenSpektrum);

        // -------------------------------------------------------------------------------------------------------------------------------------

        fftshift(amplitudenSpektrum,amplitudenSpektrum);
        cv::imshow("Geshiftetes Spektrum",amplitudenSpektrum);


        cv::Mat filter;

        cv::threshold(amplitudenSpektrum,filter,0.545,1,cv::THRESH_BINARY);
        cv::Mat inv(filter.rows,filter.cols,filter.type(),cv::Scalar(1));
        filter = inv - filter;
        filter(cv::Rect(226,204,60,106)) = 1.0;
        cv::imshow("Generierter Filter",filter);

        cv::Mat filterIMG = amplitudenSpektrum.mul(filter);
        cv::imshow("Filter_img",filterIMG);

        fftshift(filterIMG,filterIMG);

        planes[0] = planes[0].mul(filterIMG);
        planes[1] = planes[1].mul(filterIMG);

        cv::merge(planes,2,complexI);

        cv::Mat ergebnisBild;
        cv::idft(complexI,ergebnisBild,cv::DFT_REAL_OUTPUT);

        cv::normalize(ergebnisBild,ergebnisBild,1,255,CV_MINMAX);

        ergebnisBild.convertTo(ergebnisBild,CV_8U);

        cv::namedWindow("Ergebnis",cv::WINDOW_AUTOSIZE | cv::WINDOW_GUI_EXPANDED);
        cv::imshow("Ergebnis",ergebnisBild);
}





