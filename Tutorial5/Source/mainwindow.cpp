#include "Header/mainwindow.h"
#include "Header/geometry.h"
void MainWindow::setupMenuBarOwn(){
    QMenuBar *menuBar = new QMenuBar;
    QMenu *fileMenu = new QMenu("Datei");
    menuBar->addMenu(fileMenu);
    fileMenu->addAction("&Bild laden...",this,SLOT(loadImage()));
    //fileMenu->addAction("Bild speichern...");
    fileMenu->addSeparator();
    fileMenu->addAction("Beenden",this,SLOT(exitProgramm()));

    this->setMenuBar(menuBar);

}

void MainWindow::loadImage(){
    QString file;
    file = QFileDialog::getOpenFileName(this,
        tr("Bild auswählen"), "/home/andrei/Bilder/BV", tr("Bilddateien (*.png *.jpg )"));


    if(!file.isEmpty()){
        imageContainer.loadImage(file.toStdString());
        imageContainer.showOriginalImage();
        //toDoubleAndNormalize(imageContainer.image)

    }



}

void MainWindow::exitProgramm(){
    QApplication::quit();
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    this->setFixedSize(100,200);
    this->setupMenuBarOwn();
    matchKeyPointAndDisplay();

}

MainWindow::~MainWindow()
{
}

