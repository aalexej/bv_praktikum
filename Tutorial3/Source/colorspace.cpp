#include "Header/colorspace.h"
#include "Header/imagetools.h"



void ConvertImageToHSV(cv::Mat image,cv::Mat &dest)
{
    cv::cvtColor(image,dest,CV_BGR2HSV);
}

void ConvertImageToLab(cv::Mat image,cv::Mat &dest){
    cv::cvtColor(image,dest,CV_BGR2Lab);
}

void ConvertImageToCMY(cv::Mat image,cv::Mat &dest){
    cv::Mat RGBImage;
    cv::cvtColor(image,RGBImage,CV_BGR2RGB);
    int ROWS = RGBImage.rows;
    int COLS = RGBImage.cols;

    cv::Vec3b white(255,255,255);
    cv::Mat CMYimage(RGBImage.rows,RGBImage.cols,CV_8UC3);

    for(int row = 0; row < ROWS; row++){
        for(int col = 0; col < COLS;col++){
            CMYimage.at<cv::Vec3b>(row,col) = white - RGBImage.at<cv::Vec3b>(row,col);
        }
    }
    dest = CMYimage.clone();
}

void ConvertImageToYIQ(cv::Mat image,cv::Mat &dest){
    const int R = 0;
    const int G = 1;
    const int B = 2;
    cv::Matx33f YIQmat(0.299, 0.587, 0.114, 0.595716, -0.274453, -0.321263, 0.211456, -0.522591, 0.311135);
    cv::Mat RGBImage;
    cv::cvtColor(image,RGBImage,CV_BGR2RGB);
    cv::Mat YIQimage(RGBImage.rows,RGBImage.cols,CV_32FC3);
    int ROWS = RGBImage.rows;
    int COLS = RGBImage.cols;
    for(int row = 0; row < ROWS; row++ ){
        for(int col = 0; col < COLS; col++){
            cv::Vec3f rgb(RGBImage.at<cv::Vec3b>(row,col)[R] / 255,
                          RGBImage.at<cv::Vec3b>(row,col)[G] / 255,
                          RGBImage.at<cv::Vec3b>(row,col)[B] / 255);

            YIQimage.at<cv::Vec3f>(row,col) = YIQmat * rgb;
    }
  }
    dest = YIQimage.clone();
}

