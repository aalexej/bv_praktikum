### Qt Creator Cheatsheet

#### Example of Signals and Slots

```cpp
#include <QApplication>
#include <QPushButton>

int main(int argc,char *argv[]c){
    QApplication app(argc,argv);
    QPushButton hello("Ende");
    hello.show();
    QObject::connect (&hello, SIGNAL( clicked() ), &app, SLOT( quit() ));
    return app.exec();
}

```

#### QObject Klasse zum Empfangen von Signals

```cpp
// myclass.h
class MyClass : public QObject{
    O_OBJECT
    public:
        //Konstruktor
        MyClass();
        int value() const {return val;}    
    public slots:
        // Der Wert von "val" wird geändert
        void setValue(int);
    signals:
        // Das Signal soll ausgesandt werden, 
        // wenn "val" geändert wird.
        void valueChanged(int);
    private:
        int val;
}
```

##### Signal zurückgeben
Wird ein Slot aufgerufen, dann kann man mit einem Signal signalisieren, dass sich etwas verändert hat.
```cpp
// myclass.cpp
void setValue(int newValue){
    if(newValue != val){
        val = newValue;
        emit valueChanged(val);
    }
}

```