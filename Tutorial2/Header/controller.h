#ifndef CONTROLLER_H
#define CONTROLLER_H
#include <QObject>
#include <opencv2/opencv.hpp>
class Controller : public QObject {
    public:
        Controller();
    public slots:
        void openNewImage(std::string);
        void stretchHistogramm(int,int,int);
        void changeGama(int,int,int);
        void equalizeHistogramm();
        void stretchWithGamma(int,int,int);
        void stretchWithEqualize(int,int,int);

    private:
        cv::Mat image;
        std::string pathToImage;




};

#endif // CONTROLLER_H
