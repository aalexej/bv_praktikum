#ifndef HISTOGRAM_H
#define HISTOGRAM_H
#endif // HISTOGRAM_H

#include <opencv2/opencv.hpp>
#include <QChart>
#include <QBarSet>
#include <QBarSeries>
#include <QStringList>
#include <QBarCategoryAxis>
#include <QValueAxis>
#include <QLineSeries>
#include <QAreaSeries>
#include "imagetools.h"

/**
 * @brief createChartFromHistogramm creates a new chart on base from the Histogramm
 * @param chart empty chart
 */
QtCharts::QChart* createChartFromHistogramm(cv::Mat image);

/**
 * @brief histogramstretching Führt eine Grauwertspreizung durch
 * @param src Bild zur Verarbeitung
 * @param wmin Minimal gewuenschter Grauwert
 * @param wmax Maximal gewuenschter Grauwert
 * @param gamma
 */
void histogramstretching(cv::Mat &src, uint8_t wmin, uint8_t wmax, float gamma);

/**
 * @brief equalizeHist Führt eine Histogrammlinearisierung durch
 * @param src Bild zur Verarbeitung
 */
void equalizeHist(cv::Mat &src);


