QT       += quick charts widgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# OpenCV
INCLUDEPATH += /usr/local/include/opencv
LIBS += $(shell pkg-config opencv --libs)

SOURCES += \
    Source/colorspace.cpp \
    Source/convolutionwidget.cpp \
    Source/histogram.cpp \
    Source/imagetools.cpp \
    Source/main.cpp \
    Source/mainwindow.cpp \

HEADERS += \
    Header/controller.h \
    Header/convolutionwidget.h \
    Header/colorspace.h \
    Header/histogram.h \
    Header/imagetools.h \
    Header/mainwindow.h

FORMS += \
    mainwindow.ui \

qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES +=

DISTFILES +=
