#ifndef IMAGETOOLS_H
#define IMAGETOOLS_H

#endif // IMAGETOOLS_H
#include <opencv2/opencv.hpp>

struct ImageWithChannels {
    cv::Mat image;
    std::vector<cv::Mat> channels;
};

/**
 * @brief DisplayImageSelectedImage zeigt ein Bild an
 * @param image ist ein eingelesenes Bild mithilfe von openCV::imread im Format openCV::Mat
 */
void DisplayImageSelectedImage(const cv::Mat &image);
/**
 * @brief GetChannelsFromImage
 * @param imgChannels Container mit einem vorhanden Bild und einem leeren Kanal-Vektor
 */
void GetChannelsFromImage(ImageWithChannels &imgChannels);

/**
 * @brief DisplayAllChannelsFromImage Zeigt alle Kanäle mit imshow an
 * @param imgChannels Container mit Bild und allen Kanälen des Bildes
 */
void DisplayAllChannelsFromImage(ImageWithChannels &imgChannels);

/**
 * @brief MergeChannels Verbindet alle Farbkanäle zu einem Bild
 * @param imgChannels Container mit vorhandenen Kanälen und einer leeren Bild-Matrix
 */
void MergeChannels(ImageWithChannels &imgChannels);

/**
 * @brief DisplayAnaglyphImage Nimmt zwei Bilder und erstellt daraus ein Anaglyph
 * @param pathToLeftImage Pfad zum linken Bild
 * @param pathToRightImage Pfad zum rechten Bild
 */
void DisplayAnaglyphImage(cv::String pathToLeftImage, cv::String pathToRightImage);














