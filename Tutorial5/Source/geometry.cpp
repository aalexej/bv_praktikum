#include <Header/geometry.h>
#include <QMessageBox>
/*
 *  1. Lesen Sie das Bild in Ihrem Programm ein
 *  2. Finden Sie ubereinstimmende Features zwischen den Bildern ¨
 *  3. Schatzen Sie die Transformation
 *  4. Berechnen Sie die Skalierung und den Rotationswinkel
 *  5. Stellen Sie das Originalbild wieder her
 */

void displayMatchingCoordinates(std::vector<KeyPoint> keypoints1,std::vector<KeyPoint> keypoints2, std::vector<DMatch> good_matches) {
    QMessageBox msgBox;
    QString information;
    information.sprintf("KeyPoint1 x{%f} y{%f}\n KeyPoint2 x{%f} y{%f}",
                                    keypoints1[good_matches[0].queryIdx].pt.x,
                                    keypoints1[good_matches[0].queryIdx].pt.y,
                                    keypoints2[good_matches[0].trainIdx].pt.x,
                                    keypoints2[good_matches[0].trainIdx].pt.y);


    msgBox.setText(information);
    msgBox.exec();
}

Mat getAffineTransformationFromPair(ImageKeyPointMatchPairs &pairs){
    int NUMBER_OF_COMMON_PAIRS = 3;
    std::vector<Point2f> img1Points;
    std::vector<Point2f> img2Points;
    if (pairs.good_matches.size() >= NUMBER_OF_COMMON_PAIRS){
        for (int pair = 0; pair < NUMBER_OF_COMMON_PAIRS; pair++){
            KeyPoint keyPoint1 = pairs.keypoints1[pairs.good_matches[pair].queryIdx];
            KeyPoint keyPoint2 =  pairs.keypoints2[pairs.good_matches[pair].trainIdx];

            img1Points.push_back(Point2f(keyPoint1.pt.x,keyPoint1.pt.y));
            img2Points.push_back(Point2f(keyPoint2.pt.x,keyPoint2.pt.y));
        }
    }

    Mat transformation = getAffineTransform(img2Points,img1Points);
    return transformation;
}

void drawGoodMatches(ImageKeyPointMatchPairs &pairs){
    //-- Draw matches
    Mat img_matches;
    drawMatches( pairs.imgOrig1, pairs.keypoints1, pairs.imgOrig2, pairs.keypoints2, pairs.good_matches, img_matches, Scalar::all(-1),
                 Scalar::all(-1), std::vector<char>(), DrawMatchesFlags::DEFAULT | DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );

    //-- Show detected matches
    namedWindow("Good Matches",WINDOW_AUTOSIZE | WINDOW_GUI_EXPANDED);
    imshow("Good Matches", img_matches );
}

void matchKeyPointAndDisplay(){
    int minHessian = 400;

    Mat imgOrig1 = imread("/home/andrei/Bilder/BV/P5/normal.png");
    Mat imgOrig2 = imread("/home/andrei/Bilder/BV/P5/rotska.png");

    Mat img1;
    Mat img2;

    cvtColor(imgOrig1,img1,COLOR_BGR2GRAY);
    cvtColor(imgOrig2,img2,COLOR_BGR2GRAY);

    Ptr<SURF> detector = SURF::create( minHessian );
    std::vector<KeyPoint> keypoints1, keypoints2;
    Mat descriptors1, descriptors2;
    detector->detectAndCompute( img1, noArray(), keypoints1, descriptors1 );
    detector->detectAndCompute( img2, noArray(), keypoints2, descriptors2 );
    //-- Step 2: Matching descriptor vectors with a FLANN based matcher
    // Since SURF is a floating-point descriptor NORM_L2 is used
    Ptr<DescriptorMatcher> matcher = DescriptorMatcher::create(DescriptorMatcher::FLANNBASED);
    std::vector< std::vector<DMatch> > knn_matches;
    matcher->knnMatch( descriptors1, descriptors2, knn_matches, 2 );
    //-- Filter matches using the Lowe's ratio test
    const float ratio_thresh = 0.2f;
    std::vector<DMatch> good_matches;
    for (size_t i = 0; i < knn_matches.size(); i++)
    {
        if (knn_matches[i][0].distance < ratio_thresh * knn_matches[i][1].distance)
        {
            good_matches.push_back(knn_matches[i][0]);
        }
    }

    ImageKeyPointMatchPairs pairs;
    pairs.keypoints1 = keypoints1;
    pairs.keypoints2 = keypoints2;
    pairs.good_matches = good_matches;
    pairs.imgOrig1 = imgOrig1;
    pairs.imgOrig2 = imgOrig2;
    drawGoodMatches(pairs);
    Mat transformation = getAffineTransformationFromPair(pairs);

    cv::Mat result = Mat(img1.rows,img1.cols,CV_8UC3);
    warpAffine(imgOrig2,result,transformation,Size(result.size()));
    namedWindow("Backtransformation");
    imshow("Backtransformation",result);
}
