#include "Header/CVImageContainer.h"

void CVImageContainer::loadImage(std::string path){
    this->image = cv::imread(path,cv::IMREAD_GRAYSCALE);
    this->changedImage = this->image.clone();
    this->imagesLoaded = true;
}


void CVImageContainer::showOriginalImage(){
    if(imagesLoaded){
        cv::imshow("Original Image",this->image);
    }

}
void CVImageContainer::showChangedImage(){
    if(imagesLoaded){
        cv::imshow("Changed Image",this->changedImage);
    }
}
