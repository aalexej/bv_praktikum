#ifndef CVIMAGECONTAINER_H
#define CVIMAGECONTAINER_H
#include <opencv2/opencv.hpp>
#endif // CVIMAGECONTAINER_H


class CVImageContainer {

public:
    void loadImage(std::string path);
    cv::Mat image;
    cv::Mat changedImage;


    void showOriginalImage();
    void showChangedImage();

    bool imagesLoaded;

};
