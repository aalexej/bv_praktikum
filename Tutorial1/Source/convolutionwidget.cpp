#include "Header/convolutionwidget.h"

/**
 * 1. Implementieren Sie eine eigene Funktion, die zu einem Filter h die Fault mit einem Bild berrechnet.
 *    Behandeln Sie die Randprobleme, indem Sie die fehlenden Werte mit 0 auffüllen.
 *
 *    Bei welchem Filter muss das Bild unbedingt in einen anderen Datentyp umgewandelt werden, da die Filterergebnisse auch
 *    negative Werte enthalten können.
 *
 *
 * 2. Entwickeln Sie eine GUI, um ein beliebiges Bild im Ortsraum zu filtern. Dabei soll über ein Menü ein Filter ausgewählt werden.
 *    Beachten Sie, dass das Bild vor der Faltung nach double konvertiert werden muss. (image.convertTo(image,CV64FC1)
 *
 *    a) Summenfilter
 *          - Nomierter Mittelwertsfilter mit Eingabe der Filtergröße
 *          - Gaussfilter mit Eingabe der Filtergröße , OpenCV: GaussianBlur
 *
 *    b) Differenzfilter
 *
 *          - 1.te x-Ableitung als Vorwärtsgradient
 *          - 1.te y-Ableitung als Rückwärtsgradient
 *          - Laplace (OpenCV: Laplacian())
 *          - Sobel in x(=Gx), OpenCV: Sobel()
 *          - Sobel in y(=Gy), Parameter der Funktion anpassen
 *          - Gradientenbetrag nach Sobel, Formel auf Folie 7-49
 *            OpenCV: Nutzen Sie die Formel, Gx und Gy können mit Sobel() berrechnet werden
 *          - Canny Edge Detector. (OpenCV: canny())
 *
 */



void ConvolutionWidget::customConvolution(){

}
void ConvolutionWidget::mittelwertsfilter(){

}
void ConvolutionWidget::gaussfilter(){

}
void ConvolutionWidget::vorwaertsgradient(){

}
void ConvolutionWidget::rueckwaertsgradient(){

}
void ConvolutionWidget::laplace(){

}
void ConvolutionWidget::sobelGx(){

}
void ConvolutionWidget::sobelGy(){

}
void ConvolutionWidget::gradientenbetrag(){

}
void ConvolutionWidget::cannyEdgeDetector(){

}


void ConvolutionWidget::setupUi(){
    QHBoxLayout *widgetLayout = new QHBoxLayout;
    QVBoxLayout *preferencesLayout = new QVBoxLayout;

    QVBoxLayout *imagesLayout = new QVBoxLayout;


    //Preferences
    //Eigene Filter
    QGroupBox *eigeneFilterGroup = new QGroupBox("Eigene Filter");
    QHBoxLayout *eigeneFilterLayout = new QHBoxLayout;
    QPushButton *customFilterPushButton = new QPushButton("Faltung durchführen");
    QComboBox *filterWahlComboBox = new QComboBox;

    eigeneFilterLayout->addWidget(customFilterPushButton);
    eigeneFilterLayout->addWidget(filterWahlComboBox);
    eigeneFilterGroup->setLayout(eigeneFilterLayout);

    //Summenfilter
    QGroupBox *summenFilterGroup = new QGroupBox("Summenfilter");
    QVBoxLayout *summenFilterLayout = new QVBoxLayout;
    QPushButton *mittelwertfilterPushButton = new QPushButton("Mittlelwertfilter");
    QPushButton *gaussfilterPushButton = new QPushButton("Gaussfilter");

    summenFilterLayout->addWidget(mittelwertfilterPushButton);
    summenFilterLayout->addWidget(gaussfilterPushButton);
    summenFilterGroup->setLayout(summenFilterLayout);

    //Differenzfilter
    QGroupBox *differenzFilterGroup = new QGroupBox("Differenzfilter");
    QVBoxLayout *differenzFilterLayout = new QVBoxLayout;
    QPushButton *vorwaertsgradientPushButton = new QPushButton("Vorwärtsgradient");
    QPushButton *rueckwaertsgradientPushButton = new QPushButton("Rückwärtsgradient");
    QPushButton *laplacePushButton = new QPushButton("Laplacefilter");
    QPushButton *sobelGxPushButton = new QPushButton("Sobelfilter nach X");
    QPushButton *sobelGyPushButton = new QPushButton("Sobelfilter nach Y");
    QPushButton *gradientenBetragPushButton = new QPushButton("Sobelfilter Gradientenbetrag");
    QPushButton *cannyEdgePushButton = new QPushButton("Cannyedgefilter");

    differenzFilterLayout->addWidget(vorwaertsgradientPushButton);
    differenzFilterLayout->addWidget(rueckwaertsgradientPushButton);
    differenzFilterLayout->addWidget(laplacePushButton);
    differenzFilterLayout->addWidget(sobelGxPushButton);
    differenzFilterLayout->addWidget(sobelGyPushButton);
    differenzFilterLayout->addWidget(gradientenBetragPushButton);
    differenzFilterLayout->addWidget(cannyEdgePushButton);


    differenzFilterGroup->setLayout(differenzFilterLayout);


    preferencesLayout->addWidget(eigeneFilterGroup);
    preferencesLayout->addWidget(summenFilterGroup);
    preferencesLayout->addWidget(differenzFilterGroup);
    preferencesLayout->setAlignment(Qt::AlignTop|Qt::AlignLeft);

    convolutedImage = new CVImageWidget;
    imagesLayout->addWidget(convolutedImage);


    widgetLayout->addLayout(preferencesLayout);
    widgetLayout->addLayout(imagesLayout);
    this->setLayout(widgetLayout);
}


void ConvolutionWidget::loadImage(cv::Mat image){
    this->cvOriginal = image.clone();
    this->convolutedImage->showImage(this->cvOriginal);
    this->convoluted = image.clone();
}




















