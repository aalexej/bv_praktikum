### OpenCV Cheatsheet
---

#### cv::Mat Typen

```cpp
/*
    Types of Mat
    +--------+----+----+----+----+------+------+------+------+
    |        | C1 | C2 | C3 | C4 | C(5) | C(6) | C(7) | C(8) |
    +--------+----+----+----+----+------+------+------+------+
    | CV_8U  |  0 |  8 | 16 | 24 |   32 |   40 |   48 |   56 |
    | CV_8S  |  1 |  9 | 17 | 25 |   33 |   41 |   49 |   57 |
    | CV_16U |  2 | 10 | 18 | 26 |   34 |   42 |   50 |   58 |
    | CV_16S |  3 | 11 | 19 | 27 |   35 |   43 |   51 |   59 |
    | CV_32S |  4 | 12 | 20 | 28 |   36 |   44 |   52 |   60 |
    | CV_32F |  5 | 13 | 21 | 29 |   37 |   45 |   53 |   61 |
    | CV_64F |  6 | 14 | 22 | 30 |   38 |   46 |   54 |   62 |
    +--------+----+----+----+----+------+------+------+------+
 */

```

##### Umwandlung von TYP A -> TYP B

```cpp
cv::Mat imgA;
cv::Mat destination;
imgA.convertTo(imgA,destination,TYP);

```
---
#### Kanaloperationen

##### Kanäle aus einem Bild extrahieren

```cpp
void GetChannelsFromImage(ImageWithChannels &imgChannels) {
    ImageWithChannels channelContainer;
    cv::split(imgChannels.image,imgChannels.channels);
}
```

##### Kanäle zu einem Bild zusammenfassen

```cpp
void MergeChannels(ImageWithChannels &imgChannels){
    cv::merge(imgChannels.channels, imgChannels.image);
}
```
---
#### Farbraumoperationen

##### Farbraumumwandlung

```cpp
cv::Mat image;
cv::Mat dest;
cv::cvtColor(image,dest,CV_color2color);
```