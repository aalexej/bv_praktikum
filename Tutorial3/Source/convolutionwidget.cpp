#include "Header/convolutionwidget.h"
#include "Header/histogram.h"

/**
 * 1. Implementieren Sie eine eigene Funktion, die zu einem Filter h die Fault mit einem Bild berrechnet.
 *    Behandeln Sie die Randprobleme, indem Sie die fehlenden Werte mit 0 auffüllen.
 *
 *    Bei welchem Filter muss das Bild unbedingt in einen anderen Datentyp umgewandelt werden, da die Filterergebnisse auch
 *    negative Werte enthalten können.
 *
 *
 * 2. Entwickeln Sie eine GUI, um ein beliebiges Bild im Ortsraum zu filtern. Dabei soll über ein Menü ein Filter ausgewählt werden.
 *    Beachten Sie, dass das Bild vor der Faltung nach double konvertiert werden muss. (image.convertTo(image,CV64FC1)
 *
 *    a) Summenfilter
 *          - Nomierter Mittelwertsfilter mit Eingabe der Filtergröße
 *          - Gaussfilter mit Eingabe der Filtergröße , OpenCV: GaussianBlur
 *
 *    b) Differenzfilter
 *
 *          - 1.te x-Ableitung als Vorwärtsgradient
 *          - 1.te y-Ableitung als Rückwärtsgradient
 *          - Laplace (OpenCV: Laplacian())
 *          - Sobel in x(=Gx), OpenCV: Sobel()
 *          - Sobel in y(=Gy), Parameter der Funktion anpassen
 *          - Gradientenbetrag nach Sobel, Formel auf Folie 7-49
 *            OpenCV: Nutzen Sie die Formel, Gx und Gy können mit Sobel() berrechnet werden
 *          - Canny Edge Detector. (OpenCV: canny())
 *
 */

/**
 * Filter 1 :
 *             [[ 1  1 1]
 *              [ 1 -8 1]
 *              [ 1  1 1]]
 *
 * Filter 2 :
 *             [[ 1 2 1]
 *        1/16* [ 2 4 2]
 *              [ 1 2 1]]
 *
 * Filter 3:
 *             [[ 1  2  1]
 *              [ 0  0  0]
 *              [-1 -2 -1]]
 */


cv::Mat ConvolutionWidget::filterOne() {
    cv::Mat kernel = (cv::Mat_<float>(3,3) << 1 , 1 , 1,
                                              1, -8,  1,
                                              1,  1  ,1);

    return kernel;
}

cv::Mat ConvolutionWidget::filterTwo() {
    cv::Mat kernel = (cv::Mat_<float>(3,3) << 1, 2, 1,
                                              2, 4, 2,
                                              1, 2, 1);

    cv::divide(1.0/16,kernel,kernel);
    return kernel;

}

cv::Mat ConvolutionWidget::filterThree() {
    cv::Mat kernel = (cv::Mat_<float>(3,3) << -1, -2, -1,
                                               0,  0,  0,
                                               1,  2,  1);

    return kernel;
}


void ConvolutionWidget::customConvolution(){
    int const FILTER1 = 0;
    int const FILTER2 = 1;
    int const FILTER3 = 2;


    int ROWS = this->cvOriginal.rows;
    int COLS = this->cvOriginal.cols;

    cv::Mat larger = cv::Mat::zeros(this->cvOriginal.size(),CV_8UC1);
    this->convoluted = cv::Mat::zeros(this->cvOriginal.size(),CV_32FC1);
    cv::copyMakeBorder(this->cvOriginal,larger,1,1,1,1,cv::BORDER_CONSTANT);

    cv::Mat kernel;
    switch(filterWahlComboBox->currentIndex()){
        case FILTER1:
            kernel = filterOne();
        break;
        case FILTER2:
            kernel =filterTwo();
        break;
        case FILTER3:
            kernel = filterThree();
        break;
    }

    for(int row = 0; row < ROWS; row++){
        for(int col = 0; col < COLS; col++){
            float newPixel =
                    larger.at<uint8_t>(row,col)     * kernel.at<float>(0,0)+
                    larger.at<uint8_t>(row,col+1)   * kernel.at<float>(0,1)+
                    larger.at<uint8_t>(row,col+2)   * kernel.at<float>(0,2)+
                    larger.at<uint8_t>(row+1,col)   * kernel.at<float>(1,0)+
                    larger.at<uint8_t>(row+1,col+1) * kernel.at<float>(1,1)+
                    larger.at<uint8_t>(row+1,col+2) * kernel.at<float>(1,2)+
                    larger.at<uint8_t>(row+2,col)   * kernel.at<float>(2,0)+
                    larger.at<uint8_t>(row+2,col+1) * kernel.at<float>(2,1)+
                    larger.at<uint8_t>(row+2,col+2) * kernel.at<float>(2,2);

            this->convoluted.at<float>(row,col) = newPixel;
        }
    }


    histogramstretching(this->convoluted,0,255,1);
    displayConvolutedImage();

}

void ConvolutionWidget::blurFilterInputs(ConvolutionWidget::FilterInputs &input)
{
    int FILTERHEIGHT = -1;
    int FILTERWIDTH = -1;

    FILTERHEIGHT = QInputDialog::getInt(this,tr("Filterhöhe auswählen"),tr("FILTERHEIGHT: "),1,1,this->cvOriginal.cols,1);
    FILTERWIDTH = QInputDialog::getInt(this,tr("Filterbreite wählen"),"FILTERWIDTH: ",1,1,this->cvOriginal.rows,1);

    if(FILTERWIDTH != -1 || FILTERHEIGHT != -1){

        if (FILTERWIDTH % 2 == 0){
            FILTERWIDTH--;
        }

        if(FILTERHEIGHT % 2 == 0){
            FILTERHEIGHT--;
        }

        input.FILTERWIDTH = FILTERWIDTH;
        input.FILTERHEIGHT = FILTERHEIGHT;
        input.ACCEPT = true;
    }
}

void ConvolutionWidget::mittelwertsfilter(){
    FilterInputs inputs;
    blurFilterInputs(inputs);

    if(inputs.ACCEPT){
        try {
            this->convoluted.convertTo(this->convoluted,CV_64FC1);
            cv::blur(this->convoluted,this->convoluted,
                     cv::Size(inputs.FILTERWIDTH,inputs.FILTERHEIGHT));
             this->convoluted.convertTo(this->convoluted,CV_8UC1);
             displayConvolutedImage();
        } catch (cv::Exception ex) {
            QMessageBox::critical(this,"Exception","Fehler");
        }
    }
}

void ConvolutionWidget::gaussfilter(){
    FilterInputs inputs;
    blurFilterInputs(inputs);

    if(inputs.ACCEPT){
        try {
            this->convoluted.convertTo(this->convoluted,CV_64FC1);
            cv::GaussianBlur(this->convoluted,
                             this->convoluted,
                             cv::Size(inputs.FILTERWIDTH,inputs.FILTERHEIGHT),
                             1.0,0,cv::BORDER_DEFAULT);

            this->convoluted.convertTo(this->convoluted,CV_8UC1);
            displayConvolutedImage();
        } catch (cv::Exception ex) {
           QMessageBox::critical(this,"Exception","Fehler");

        }
    }
}

void ConvolutionWidget::vorwaertsgradient(){

    int ddepth = CV_8UC1;
    cv::Mat kernel = (cv::Mat_<float>(3,1) << 0 , 1 , -1 );
    cv::filter2D(this->cvOriginal,this->convoluted,ddepth,kernel);
    displayConvolutedImage();

}

void ConvolutionWidget::rueckwaertsgradient(){

    int ddepth = CV_8UC1;
    cv::Mat kernel = (cv::Mat_<float>(1,3) << 1,- 1, 0 );
    cv::filter2D(this->cvOriginal,this->convoluted,ddepth,kernel);
    displayConvolutedImage();

}

void ConvolutionWidget::laplace(){
      int kernel_size = 3;
      int scale = 1;
      int delta = 0;
      int ddepth = CV_8UC1;
      cv::Laplacian(this->cvOriginal,this->convoluted,ddepth,kernel_size,scale,delta,cv::BORDER_DEFAULT);
      displayConvolutedImage();

}
void ConvolutionWidget::sobelGx(){
    cv::Mat dst;

    cv::Sobel(this->convoluted,
              dst,
              CV_16S,
              2,
              0);
    dst.convertTo(dst,CV_8UC1);
    this->convoluted = dst.clone();
    displayConvolutedImage();
}
void ConvolutionWidget::sobelGy(){
    cv::Mat dst;
    cv::Sobel(this->convoluted,
              dst,
              CV_16S,
              0,
              2);
    dst.convertTo(dst,CV_8UC1);
    this->convoluted = dst.clone();
    displayConvolutedImage();
}
void ConvolutionWidget::gradientenbetrag(){
    cv::Mat src_gray = this->convoluted.clone();
    cv::Mat grad;
    int scale = 1;
    int delta = 0;
    int ddepth = CV_16S;
    GaussianBlur( src_gray, src_gray, cv::Size(3,3), 0, 0, cv::BORDER_DEFAULT );

    cv::Mat grad_x, grad_y;
    cv::Mat abs_grad_x, abs_grad_y;

    /// Gradient X
    cv::Sobel( src_gray, grad_x, ddepth, 1, 0, 3, scale, delta, cv::BORDER_DEFAULT );
    /// Gradient Y
    cv::Sobel( src_gray, grad_y, ddepth, 0, 1, 3, scale, delta, cv::BORDER_DEFAULT );

    cv::convertScaleAbs( grad_x, abs_grad_x );
    cv::convertScaleAbs( grad_y, abs_grad_y );

    cv::addWeighted( abs_grad_x, 0.5, abs_grad_y, 0.5, 0, grad );
    grad.convertTo(this->convoluted,CV_8U);
    this->displayConvolutedImage();

}

void ConvolutionWidget::cannyEdgeDetector(){
    cv::Mat dst, detected_edges;
    int lowThreshold = QInputDialog::getInt(this,
                                            "Cannyedge Detector",
                                            "Treshold eingeben:",
                                            1,
                                            1,
                                            100,1);
    int ratio = 3;
    int kernel_size = 3;
    dst.create(this->convoluted.size(),this->convoluted.type());
    cv::blur(this->cvOriginal,detected_edges,cv::Size(5,5));
    cv::Canny(detected_edges,detected_edges,lowThreshold,lowThreshold*ratio, kernel_size);
    dst = cv::Scalar::all(0);
    convoluted.copyTo(dst,detected_edges);
    convoluted = dst.clone();
    displayConvolutedImage();
}


void ConvolutionWidget::setupUi(){

    QVBoxLayout *preferencesLayout = new QVBoxLayout;

    //Preferences
    //Eigene Filter
    QGroupBox *eigeneFilterGroup = new QGroupBox("Eigene Filter");
    QHBoxLayout *eigeneFilterLayout = new QHBoxLayout;
    QPushButton *customFilterPushButton = new QPushButton("Faltung durchführen");
    filterWahlComboBox = new QComboBox;
    filterWahlComboBox->addItem("Filter 1");
    filterWahlComboBox->addItem("Filter 2");
    filterWahlComboBox->addItem("Filter 3");


    eigeneFilterLayout->addWidget(customFilterPushButton);
    eigeneFilterLayout->addWidget(filterWahlComboBox);
    eigeneFilterGroup->setLayout(eigeneFilterLayout);

    connect(customFilterPushButton,SIGNAL(clicked()),this,SLOT(customConvolution()));

    //Summenfilter
    QGroupBox *summenFilterGroup = new QGroupBox("Summenfilter");
    QVBoxLayout *summenFilterLayout = new QVBoxLayout;
    QPushButton *mittelwertfilterPushButton = new QPushButton("Mittlelwertfilter");
    QPushButton *gaussfilterPushButton = new QPushButton("Gaussfilter");
    connect(mittelwertfilterPushButton,SIGNAL(clicked()),this,SLOT(mittelwertsfilter()));
    connect(gaussfilterPushButton,SIGNAL(clicked()),this,SLOT(gaussfilter()));

    summenFilterLayout->addWidget(mittelwertfilterPushButton);
    summenFilterLayout->addWidget(gaussfilterPushButton);
    summenFilterGroup->setLayout(summenFilterLayout);

    //Differenzfilter
    QGroupBox *differenzFilterGroup = new QGroupBox("Differenzfilter");
    QVBoxLayout *differenzFilterLayout = new QVBoxLayout;
    QPushButton *vorwaertsgradientPushButton = new QPushButton("Vorwärtsgradient");
    QPushButton *rueckwaertsgradientPushButton = new QPushButton("Rückwärtsgradient");
    QPushButton *laplacePushButton = new QPushButton("Laplacefilter");
    QPushButton *sobelGxPushButton = new QPushButton("Sobelfilter nach X");
    QPushButton *sobelGyPushButton = new QPushButton("Sobelfilter nach Y");
    QPushButton *gradientenBetragPushButton = new QPushButton("Sobelfilter Gradientenbetrag");
    QPushButton *cannyEdgePushButton = new QPushButton("Cannyedgefilter");


    connect(vorwaertsgradientPushButton,SIGNAL(clicked()),this,SLOT(vorwaertsgradient()));
    connect(rueckwaertsgradientPushButton,SIGNAL(clicked()),this,SLOT(rueckwaertsgradient()));
    connect(laplacePushButton,SIGNAL(clicked()),this,SLOT(laplace()));
    connect(sobelGxPushButton,SIGNAL(clicked()),this,SLOT(sobelGx()));
    connect(sobelGyPushButton,SIGNAL(clicked()),this,SLOT(sobelGy()));
    connect(gradientenBetragPushButton,SIGNAL(clicked()),this,SLOT(gradientenbetrag()));
    connect(cannyEdgePushButton,SIGNAL(clicked()),this,SLOT(cannyEdgeDetector()));


    differenzFilterLayout->addWidget(vorwaertsgradientPushButton);
    differenzFilterLayout->addWidget(rueckwaertsgradientPushButton);
    differenzFilterLayout->addWidget(laplacePushButton);
    differenzFilterLayout->addWidget(sobelGxPushButton);
    differenzFilterLayout->addWidget(sobelGyPushButton);
    differenzFilterLayout->addWidget(gradientenBetragPushButton);
    differenzFilterLayout->addWidget(cannyEdgePushButton);


    differenzFilterGroup->setLayout(differenzFilterLayout);


    preferencesLayout->addWidget(eigeneFilterGroup);
    preferencesLayout->addWidget(summenFilterGroup);
    preferencesLayout->addWidget(differenzFilterGroup);


    QPushButton *resetImagePushButton = new QPushButton("Zurücksetzen");
    connect(resetImagePushButton,SIGNAL(clicked()),this,SLOT(resetImage()));
    preferencesLayout->addWidget(resetImagePushButton,0,Qt::AlignHCenter);

    preferencesLayout->setAlignment(Qt::AlignTop|Qt::AlignLeft);

    this->setLayout(preferencesLayout);
}
void ConvolutionWidget::loadImage(cv::Mat image){
    this->cvOriginal = image.clone();
    this->convoluted = image.clone();
    displayConvolutedImage();
}
void ConvolutionWidget::resetImage(){
   this->convoluted = cvOriginal.clone();
   displayConvolutedImage();
}

void ConvolutionWidget::displayConvolutedImage(){
    cv::imshow("Faltungsergebnis",this->convoluted);
}



















