#ifndef MORPHOLOGYTOOLS_H
#define MORPHOLOGYTOOLS_H

#include <opencv2/opencv.hpp>


/*
 *  Aufgabe A:
 *  Erzeugen Sie ein kreisf¨ormiges Stukturelement mit Radius 11. (Matlab: strel(), OpenCV: getStructuringElement())
 *  Filtern Sie nun die Zellen mit diesem Strukturelement weg. Der Hintergrund muss nach der
 *  Filterung die gleiche Gr¨oße wie vor der Filterung haben! Sie erhalten so das Hintergrundbild.
 *  Subtrahieren Sie das Hintergrundbild vom Originalbild. Beachten Sie, dass dadurch negative
 *  Zahlen auftreten k¨onnen.
 *  Wandeln Sie das Ergebnis wieder in ein Grauwertbild mit 8 Bit Tiefe um.
 *
 *  Erzeugen Sie aus dem Grauwertbild ein Bin¨arbild. Testen Sie, welcher Schwellwert gut geeignet
 *  ist. (Matlab: im2bw(), OpenCV: threshold())
 *
 *  Fuhren Sie auf dem Bin ¨ ¨arbild ein Opening mit einem 3×3 großen Strukturelement durch, welches
 *  komplett mit einsen gefullt ist. (Matlab: imopen(), OpenCV: morphologyEx()) ¨
 *
 *  Fuhren Sie nun auf dem Ergebnis ein Closing mit einem kreisf ¨ ¨ormigen Strukturelement mit
 *  Radius 4 durch. (Matlab: imclose(), OpenCV: morphologyEx())
 *
 *  Uberlagern Sie das Originalbild mit dem Bin ¨ ¨arbild. Alle Zellen, die im Bin¨arbild ubrig geblieben ¨
 *  sind, sollen mit dem Grauwert 255 im Original angezeigt werden.
 *
 *  Geben Sie das Ergebnis und die Zwischenschritte aus. Beispiele hierfur zeigen die folgenden ¨
 *  Abbildungen.
 *
 *
 */


void aufgabeA(cv::Mat image);
void aufgabeB(cv::Mat image);


#endif // MORPHOLOGYTOOLS_H
