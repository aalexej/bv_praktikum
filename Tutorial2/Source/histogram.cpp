#include "Header/histogram.h"
using namespace QtCharts;

/*
    Types of Mat
    +--------+----+----+----+----+------+------+------+------+
    |        | C1 | C2 | C3 | C4 | C(5) | C(6) | C(7) | C(8) |
    +--------+----+----+----+----+------+------+------+------+
    | CV_8U  |  0 |  8 | 16 | 24 |   32 |   40 |   48 |   56 |
    | CV_8S  |  1 |  9 | 17 | 25 |   33 |   41 |   49 |   57 |
    | CV_16U |  2 | 10 | 18 | 26 |   34 |   42 |   50 |   58 |
    | CV_16S |  3 | 11 | 19 | 27 |   35 |   43 |   51 |   59 |
    | CV_32S |  4 | 12 | 20 | 28 |   36 |   44 |   52 |   60 |
    | CV_32F |  5 | 13 | 21 | 29 |   37 |   45 |   53 |   61 |
    | CV_64F |  6 | 14 | 22 | 30 |   38 |   46 |   54 |   62 |
    +--------+----+----+----+----+------+------+------+------+
 */

void histogramstretching(cv::Mat &src, uint8_t wmin, uint8_t wmax, float gamma){
    int ROWS = src.rows;
    int COLS = src.cols;
    int TOTAL_NUMBER_CHANNELS = src.channels();
    int TYPE_OF_MAT = src.type();
    std::printf("Rows: %d\nCols: %d\nChannels: %d\nType: %d\n", ROWS,COLS,TOTAL_NUMBER_CHANNELS,TYPE_OF_MAT);
    if(TYPE_OF_MAT != CV_8U){
        src.convertTo(src,CV_8U);
    }
    std::vector<cv::Mat> channels(TOTAL_NUMBER_CHANNELS);
    cv::split(src,channels);
    for(int channel = 0; channel < TOTAL_NUMBER_CHANNELS; channel++){
        double gmin,gmax;
        cv::minMaxLoc(channels[channel],&gmin,&gmax);
        printf("Gmin: %f, Gmax: %f",gmin,gmax);
        std::cout << std::endl;
        for(int row = 0; row < ROWS; row++){
            for(int col = 0; col < COLS; col++){
                    uint8_t g_alt = channels[channel].at<uint8_t>(row,col);
                    uint8_t g_neu = (wmax-wmin) * pow(((g_alt - gmin)/(gmax-gmin)),gamma) + wmin;
                    channels[channel].at<uint8_t>(row,col) = g_neu;
            }
        }
        cv::minMaxLoc(channels[channel],&gmin,&gmax);
        printf("Gmin: %f, Gmax: %f",gmin,gmax);
        std::cout << std::endl;
    }
    //Copy all new Values from the different channels old Image
    cv::merge(channels,src);
}

void equalizeHist(cv::Mat &src){
    cv::Mat HOUT;
    cv::equalizeHist(src,HOUT);
    src = HOUT.clone();
}

QChart* createChartFromHistogramm(cv::Mat image){


    cv::Mat GrayChannel = image.clone();
    cv::MatND hist;
    int channels[] = {0};
    int histSize[] = {255};
    float vranges[] = {0,255};
    const float* ranges[] = {vranges};

    cv::calcHist(&GrayChannel,1,channels,cv::noArray(),hist,1,histSize,ranges,true,false);
    double min,max;
    cv::minMaxLoc(hist,&min,&max);
    //printf("Histogramm:\nMinimum: %f\nMaximum: %f",min,max);

    QBarSet *set0 = new QBarSet("Hellwert");
    QLineSeries *set1 = new QLineSeries();
    for(int histIndex = 0; histIndex < histSize[0]; histIndex++){
        *set0 << cvRound(hist.at<float>(histIndex));
        set1->append(histIndex,cvRound(hist.at<float>(histIndex)));
    }

    QBarSeries *series = new QBarSeries();
    QAreaSeries *series1 = new QAreaSeries();
    series->append(set0);
    series1->setUpperSeries(set1);


    QChart *chart = new QChart();
    chart->addSeries(series1);

    chart->setTitle("Histogramm");
    chart->setAnimationOptions(QChart::SeriesAnimations);

    QValueAxis *axisX = new QValueAxis();
    axisX->setRange(0,255);
    chart->addAxis(axisX, Qt::AlignBottom);
    series1->attachAxis(axisX);

    QValueAxis *axisY = new QValueAxis();
    axisY->setRange(0,max);
    chart->addAxis(axisY, Qt::AlignLeft);
    series1->attachAxis(axisY);

    chart->legend()->setVisible(true);
    chart->legend()->setAlignment(Qt::AlignBottom);

    return chart;

}

