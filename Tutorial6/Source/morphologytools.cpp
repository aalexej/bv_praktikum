#include "Header/morphologytools.h"

/*
 *  Aufgabe A:
 *  Erzeugen Sie ein kreisf¨ormiges Stukturelement mit Radius 11. (Matlab: strel(), OpenCV: getStructuringElement())
 *
 *  Filtern Sie nun die Zellen mit diesem Strukturelement weg. Der Hintergrund muss nach der
 *
 *  Filterung die gleiche Gr¨oße wie vor der Filterung haben! Sie erhalten so das Hintergrundbild.
 *
 *  Subtrahieren Sie das Hintergrundbild vom Originalbild. Beachten Sie, dass dadurch negative
 *  Zahlen auftreten k¨onnen.
 *
 *  Wandeln Sie das Ergebnis wieder in ein Grauwertbild mit 8 Bit Tiefe um.
 *
 *  Aufgabe B:
 *  Erzeugen Sie aus dem Grauwertbild ein Bin¨arbild. Testen Sie, welcher Schwellwert gut geeignet
 *  ist. (Matlab: im2bw(), OpenCV: threshold())
 *
 *  Fuhren Sie auf dem Bin ¨ ¨arbild ein Opening mit einem 3×3 großen Strukturelement durch, welches
 *  komplett mit einsen gefullt ist. (Matlab: imopen(), OpenCV: morphologyEx()) ¨
 *
 *  Fuhren Sie nun auf dem Ergebnis ein Closing mit einem kreisf ¨ ¨ormigen Strukturelement mit
 *  Radius 4 durch. (Matlab: imclose(), OpenCV: morphologyEx())
 *
 *  Uberlagern Sie das Originalbild mit dem Bin ¨ ¨arbild. Alle Zellen, die im Bin¨arbild ubrig geblieben ¨
 *  sind, sollen mit dem Grauwert 255 im Original angezeigt werden.
 *
 *  Geben Sie das Ergebnis und die Zwischenschritte aus. Beispiele hierfur zeigen die folgenden ¨
 *  Abbildungen.
 *
 *
 */



void aufgabeA(cv::Mat image){
    using namespace cv;
    int DIAMETER = 22;
    Mat structElement = getStructuringElement(MORPH_ELLIPSE,Size(DIAMETER,DIAMETER));

    Mat background(image.size(),image.type());
    dilate(image,background,structElement);
    cv::imshow("Hintergrundbild",background);

    Mat sub;
    subtract(background,image,sub);
    cv::imshow("Subtraktion mit Hintergrund",sub);
    normalize(sub,sub,0,255,CV_MINMAX,CV_8UC1);
    cv::imshow("Umwandlung auf 8-Bit Grauwert",sub);

}

void aufgabeB(cv::Mat image){
    using namespace cv;
    Mat img_bw;
    double th = threshold(image, img_bw, 155, 255, CV_THRESH_BINARY);
    imshow("Binary Image",img_bw);


    Mat strukturElementRect = getStructuringElement(MORPH_RECT,Size(3,3));
    Mat opening;
    morphologyEx(img_bw,opening,MORPH_OPEN,strukturElementRect);
    imshow("Opening",opening);

    Mat closing;
    int DURCHMESSER = 8;
    Mat strukturElementKreis = getStructuringElement(MORPH_ELLIPSE,Size(DURCHMESSER,DURCHMESSER));
    morphologyEx(opening,closing,MORPH_CLOSE,strukturElementKreis);
    imshow("Closing",closing);

    bitwise_not(closing,closing);

    Mat overlay;
    addWeighted(image,1,closing,1,0.0,overlay);

    imshow("Überlagerung mit Binärbild",overlay);
}






