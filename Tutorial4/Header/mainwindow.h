#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QApplication>
#include <opencv2/core/core.hpp>
#include "Header/CVImageContainer.h"
#include <QFileDialog>
#include <QMenuBar>
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    void setupMenuBarOwn();
    ~MainWindow();

public slots:
    void loadImage();
    void exitProgramm();


private:
    CVImageContainer imageContainer;
};
#endif // MAINWINDOW_H
