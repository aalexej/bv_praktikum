#ifndef COLORSPACE_H
#define COLORSPACE_H

#endif // COLORSPACE_H
#include <opencv2/opencv.hpp>

/**
 * @brief ConvertImageToHSV verwendet die openCV Funktion cvtColor (BGR->HSV)
 * @param image Eingangsbild
 * @param dest Ausgangsbild
 */
void ConvertImageToHSV(cv::Mat image, cv::Mat &dest);

/**
 * @brief ConvertImageToCMY (BGR->CMY)
 * @param image Eingangsbild
 * @param dest Ausgangsbild
 */
void ConvertImageToCMY(cv::Mat image,cv::Mat &dest);

/**
 * @brief ConvertImageToYIQ (BGR->YIQ)
 * @param image Eingangsbild
 * @param dest Ausgangsbild
 */
void ConvertImageToYIQ(cv::Mat image,cv::Mat &dest);

/**
 * @brief ConvertImageToLab verwendet die openCV Funtkion cvtColor (BGR->Lab)
 * @param image Eingangsbild
 * @param dest Ausgangsbild
 */
void ConvertImageToLab(cv::Mat image,cv::Mat &dest);
