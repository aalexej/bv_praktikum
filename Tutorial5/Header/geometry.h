#ifndef GEOMETRY_H
#define GEOMETRY_H
#include <opencv2/opencv.hpp>
#include <opencv2/xfeatures2d/nonfree.hpp>

#endif // GEOMETRY_H

/*
 *  1. Lesen Sie das Bild in Ihrem Programm ein
 *  2. Finden Sie ubereinstimmende Features zwischen den Bildern ¨
 *  3. Sch¨atzen Sie die Transformation
 *  4. Berechnen Sie die Skalierung und den Rotationswinkel
 *  5. Stellen Sie das Originalbild wieder her
 */

using namespace cv;
using namespace cv::xfeatures2d;

struct ImageKeyPointMatchPairs {
    std::vector<DMatch> good_matches;
    std::vector<KeyPoint> keypoints1, keypoints2;
    Mat imgOrig1;
    Mat imgOrig2;
};

void matchKeyPointAndDisplay();
