#include "Header/mainwindow.h"
#include "Header/morphologytools.h"

void MainWindow::setupMenuBarOwn(){
    QMenuBar *menuBar = new QMenuBar;
    QMenu *fileMenu = new QMenu("Datei");
    menuBar->addMenu(fileMenu);
    fileMenu->addAction("&Bild laden...",this,SLOT(loadImage()));
    //fileMenu->addAction("Bild speichern...");
    fileMenu->addSeparator();
    fileMenu->addAction("Beenden",this,SLOT(exitProgramm()));
    this->setMenuBar(menuBar);

}

void MainWindow::setupControlls(){
    this->centralWidget = new QWidget;
    this->setCentralWidget(centralWidget);
    QPushButton *aufgabeAButton = new QPushButton("Aufgabe A");
    QPushButton *aufgabeBButton = new QPushButton("Aufgabe B");
    connect(aufgabeAButton,SIGNAL(clicked()),this,SLOT(aufgabeArun()));
    connect(aufgabeBButton,SIGNAL(clicked()),this,SLOT(aufgabeBrun()));

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(aufgabeAButton);
    layout->addWidget(aufgabeBButton);

    this->centralWidget->setLayout(layout);

}

void MainWindow::loadImage(){
    QString file;
    file = QFileDialog::getOpenFileName(this,
        tr("Bild auswählen"), "/home/andrei/Bilder/BV", tr("Bilddateien (*.png *.jpg )"));


    if(!file.isEmpty()){
        imageContainer.loadImage(file.toStdString());
        imageContainer.showOriginalImage();
    }



}


void MainWindow::showWarningDialog(QString text){

    int ret = QMessageBox::warning(this,"Warnung",text);

}

void MainWindow::exitProgramm(){
    QApplication::quit();
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    this->setFixedSize(100,200);
    this->setupMenuBarOwn();
    this->setupControlls();


}

void MainWindow::aufgabeArun(){
    if(imageContainer.imagesLoaded){
        aufgabeA(imageContainer.changedImage);

    }else{
        showWarningDialog(QString("Kein Bild ausgewählt"));
    }
}
void MainWindow::aufgabeBrun(){
    if(imageContainer.imagesLoaded){
        aufgabeB(imageContainer.changedImage);
    }else{
        showWarningDialog(QString("Kein Bild ausgewählt"));
    }
}

MainWindow::~MainWindow()
{
}

