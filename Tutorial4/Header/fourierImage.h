#ifndef FOURIERIMAGE_H
#define FOURIERIMAGE_H

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc/imgproc.hpp"

#endif // FOURIERIMAGE_H


/**
 * @brief fourier Entfernt Rauschen im Bild
 * @param image Originalbild
 */
void fourier(cv::Mat image);
