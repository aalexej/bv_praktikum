#include <opencv2/opencv.hpp>
#include <Header/imagetools.h>




void DisplayImageSelectedImage(const cv::Mat &image) {
    cv::namedWindow("Display Image",cv::WINDOW_AUTOSIZE);
    cv::imshow("Display Image",image);
}


void GetChannelsFromImage(ImageWithChannels &imgChannels) {
    ImageWithChannels channelContainer;
    cv::split(imgChannels.image,imgChannels.channels);
}


void DisplayAllChannelsFromImage(ImageWithChannels &imgChannels){
    const int NUMBER_OF_CHANNELS = imgChannels.channels.size();
    for (int channel = 0; channel < NUMBER_OF_CHANNELS; channel ++ ){
            cv::imshow("Channel "+ std::to_string(channel),imgChannels.channels.at(channel));
    }
}

void MergeChannels(ImageWithChannels &imgChannels){
    cv::merge(imgChannels.channels, imgChannels.image);
}

void DisplayAnaglyphImage(cv::String pathToLeftImage, cv::String pathToRightImage) {
    const int R = 0;
    const int G = 1;
    const int B = 2;

    ImageWithChannels leftChannels, rightChannels, anaglyph;

    cv::Mat left = cv::imread(pathToLeftImage);
    cv::Mat right = cv::imread(pathToRightImage);

    cv::Mat leftRGB, rightRGB;

    cv::cvtColor(left,leftRGB,CV_BGR2RGB);
    cv::cvtColor(right,rightRGB,CV_BGR2RGB);

    leftChannels.image = leftRGB;
    rightChannels.image = rightRGB;

    GetChannelsFromImage(leftChannels);
    GetChannelsFromImage(rightChannels);

    anaglyph.channels.resize(3);
    anaglyph.channels[R] = leftChannels.channels[R];
    anaglyph.channels[G] = rightChannels.channels[G];
    anaglyph.channels[B] = rightChannels.channels[B];

    MergeChannels(anaglyph);
    cv::Mat BGRimage;
    cv::cvtColor(anaglyph.image,BGRimage,CV_RGB2BGR);

    cv::imshow("Anaglyph", BGRimage);
}


