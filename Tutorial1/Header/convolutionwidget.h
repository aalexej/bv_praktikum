#pragma once
#ifndef CONVOLUTIONWIDGET_H
#define CONVOLUTIONWIDGET_H
#endif // CONVOLUTIONWIDGET_H



#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QGroupBox>
#include <QPushButton>
#include <QComboBox>

#include "Header/cvimagewidget.h"


class ConvolutionWidget : public QWidget
{
    Q_OBJECT
public:
    explicit ConvolutionWidget(QWidget *parent = 0) : QWidget(parent) {setupUi();}
    void setupUi();


public slots:
    // Aufgabe 1
    void customConvolution();
    // Aufgabe 2.1
    void mittelwertsfilter();
    void gaussfilter();
    //Aufgabe 2.2
    void vorwaertsgradient();
    void rueckwaertsgradient();
    void laplace();
    void sobelGx();
    void sobelGy();
    void gradientenbetrag();
    void cannyEdgeDetector();
    void loadImage(cv::Mat image);

private:
    CVImageWidget *convolutedImage;
    CVImageWidget *originalImage;
    cv::Mat convoluted;
    cv::Mat cvOriginal;

};

